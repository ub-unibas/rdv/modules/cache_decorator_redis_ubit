# cache_decorator_redis_ubit

cache decorator to speed up functions which return static response, requires redis for storage

## functionalities

### usage
* use as function decorator
```
@cache_decorator
def get_test(key=Test):
    return key
 ```
* use within code
```
cache_decorator.set_keyvalue_cache(key="Test", value=2)
cache_decorator.get_keyvalue_cache(key="Test")
```

#### hints
* instantiate CacheDecorator before using it, if you use it multiple times (speed up)
* use param-variables to create useful keys when used as decorator, even if params are not used within function

### return values
the CacheDecorator tries to return an internal data structure, 
so if a json blob is stored it will be returned using json.loads()

### store empty
if a function returns a value that is false, no cache entry is created,
except if store_empty is definid within the configuration (see cache_emptyvalues.yaml)

### limit duration of existing cache:
if the duration of a cache shall be reduced you it is possible to change the cachedecorator 
to a cachedecoration with shorter period (e.g. from CacheDecorator to CacheDecorator1h)
and it reevaluates the time from when the value was cached the first time

### key-generation
the lookup key is built from the function name and the params that are passed. 
If the CacheDecorator is used on a class method, within this class a set_cache_key and get_cache_key method 
can be defined which overwrites this standard behavior (function name + params)

### logging
if the Cache Decorator is used for a class method 
and the class has a logger property this is used for logging, 
otherwise cfg/logger_cache.yaml from module is used as a fallback logging configuration

## config-files
for the predefined CacheDecorator classes (e.g. CacheDecorator1h, NoCacheDecorator) are read directly from the module 
(optimized for ubunibas infrastructure), so after modifying the cfg files the module has to be reinstalled: 
```python3 setup.py install```

ENV-Variable MODE defines which setting from config file is used: default is debug, to change this:
```export MODE=test```

as an alternative you can create your own CacheDecorator classes referencing external cfg-files:
```CacheDecorator = create_cache_decorator(config_file= "/tmp/cache.yaml")```

It is also possible to define a custom configuration like this
```
redis_geonames_config = {
    'host': 'localhost', 
    'port': 6381, 
    'cache': True, 
    'db': 5, 
    'exp_time': 90000000, 
    'store_empty': [True, None, 0, '', [], {}]
}
geonames_cache_decorator = create_cache_decorator(config_dict=redis_geonames_config)
```




## known issues
sometimes dictionaries throw errors when being pickled ("maximum recursion depth exceeded")

## example
```
from cache_decorator_redis_ubit import create_cache_decorator

CacheDecoratorTest = create_cache_decorator(config_file=/tmp/cache.yaml"))

@CacheDecorator2()
def test2():
    return set([3, 5])

test2()
```


