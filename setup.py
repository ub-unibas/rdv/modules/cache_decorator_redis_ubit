import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="cache_decorator_redis_ubit",
    setup_requires=['setuptools-git-versioning'],
    setuptools_git_versioning={
        "enabled": True,
    },
    author="Martin Reisacher",
    author_email="martin.reisacher@unibas.ch",
    description="cache decorator (using redis) to speed up function calls",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ub-unibas/rdv/modules/cache_decorator_redis_ubit",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    install_requires=[
        'pytz',
        'PyYAML',
        'redis'
    ],
    include_package_data=True,
    zip_safe=False
)