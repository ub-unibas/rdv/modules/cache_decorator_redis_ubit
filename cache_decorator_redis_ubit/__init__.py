from cache_decorator_redis_ubit.cache_decorator import create_cache_decorator
from pkg_resources import Requirement, resource_filename
import functools

CacheDecorator = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/cache.yaml"))
CacheDecoratorEmptyValues = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/cache_emptyvalues.yaml"))
# typo for backward compatibility
CacheDecoratorEmtyValues = CacheDecoratorEmptyValues
ShortCacheDecorator = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/short_cache.yaml"))
NoCacheDecorator = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/no_cache.yaml"))

CacheDecorator1h = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/1h_cache.yaml"))
CacheDecorator12h = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/12h_cache.yaml"))
CacheDecorator24h = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/24h_cache.yaml"))

EternalCacheDecorator = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/cache.yaml"))
SettingsShortCacheDecorator = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/short_cache.yaml"))

SettingsCacheDecorator1h = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/1h_cache.yaml"))
SettingsCacheDecorator12h = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/12h_cache.yaml"))
SettingsCacheDecorator24h = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/24h_cache.yaml"))

class NoCache:

    def __call__(self, fn):
        @functools.wraps(fn)
        def cache_func(*args, **kwargs):
            return_value = fn(*args, **kwargs)
            return return_value

        return cache_func