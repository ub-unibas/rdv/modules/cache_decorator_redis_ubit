from cache_decorator_redis_ubit import create_cache_decorator
from pkg_resources import Requirement, resource_filename
import time
CacheDecorator2 = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/cache.yaml"))
CacheDecoratorEmpty = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/cache_emptyvalues.yaml"))
ShortCache = create_cache_decorator(config_file=resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/short_cache.yaml"))

class A:

    @ShortCache()
    def test(self, b):
        return b

    def set_cache_key(self, b):
        return b

@ShortCache()
def test(a):
    return {"test": a}

@CacheDecorator2()
def test2():
    return set([3, 5])

@ShortCache()
def test3():
    return False

@CacheDecoratorEmpty()
def test6():
    return []

#print(test(5)["test"])
#print(test2())
#print(test3())
print(test6())