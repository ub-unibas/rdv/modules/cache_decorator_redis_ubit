import os
import json
import yaml
import pickle
import socket
import inspect
import logging.config

import redis
from datetime import datetime, timezone, timedelta
import functools
from typing import Any, Callable
from pkg_resources import Requirement, resource_filename

from redis import Redis
from pytz import reference

# todo cache problem bei mets id
# TODO: cache einen Teil der config vom RDV übergebn können

MODE = os.getenv('MODE') or "debug"
REMOTE_SERVER = "unibas.ch"
CACHEPATH = "/tmp/cache"
CACHE_DB_DICT = {0: "data_dump", 1: "gdrive", 3: "iiif"}

def create_cache_decorator(config_file = None, config_dict= {}):
    if config_file:
        config_data = yaml.load(open(config_file), Loader=yaml.SafeLoader)
    elif config_dict:
        config_data = config_dict
    else:
        config_data = yaml.load(open("cache_decorator_redis_ubit/cfg/cache.yaml"), Loader=yaml.SafeLoader)
    logger = CacheLoader.get_logger()

    if MODE in ["prod"]:
        config = config_data.get("prod",{})
    elif MODE in ["test"]:
        config = config_data.get("test",{})
    elif MODE in ["container"]:
        config = config_data.get("container",{})
    elif MODE in ["k8s"]:
        config = config_data.get("k8s",{})
    elif MODE in ["norediscache"]:
        # useful for pytests for example
        # @CacheDecorator is usable but it won't do anything
        class NoCache:
            def __call__(self, fn):
                @functools.wraps(fn)
                def cache_func(*args, **kwargs):
                    return_value = fn(*args, **kwargs)
                    return return_value

                return cache_func
        return NoCache
    else:
        config = config_data.get("debug",{})

    if not config:
        config = config_data
    #logger.debug("Cache MODE: {}, config: {}".format(MODE, config_data))

    class CacheLoaderConfig(CacheLoader):

        """
        :param socket: unix socket for redis communication
        :param cache: bool if cache shall be used
        :param db: redis db to be used for caching, 0 .. 10

        :param exp_time: exp time for cache,
            if exp time is set and not last_change_date_str each access will prolong cache livetime
        """
        _socket = config.get("socket")
        _host = config.get("host")
        _port = config.get("port")
        _cache = config.get("cache")
        _db_key = config.get("db")
        _exp_time = config.get("exp_time")
        _store_empty = config.get("store_empty", [None, False, 0, ""])



    return CacheLoaderConfig


class CacheLoader:
    # global variable to use cache (overrules all other caching options), is also set via connection test
    use_cache = False
    # to check only once if connection is available
    connection_tested = False
    # dict to store initialized redis databases
    redis_db = {}
    def __init__(self, last_change_date_str: str = "2050-01-01 01:00:00.0Z"):
        """ flexible Cache Class based on redis for various purposes
        (testing, iiif-cache, api-call cache, gspreadsheet cache)
        :param last_change_date_str: datetime in date.now.isoformat
            to check if cache was created after last_change_date_str """

        self._last_change_date = self.laststrdate2date(last_change_date_str) if last_change_date_str else \
            self.laststrdate2date("2050-01-01 01:00:00.0Z")

        # initialize redis db
        if self._db_key in self.redis_db:
            self.r = self.redis_db[self._db_key]
        else:
            self.r = self._initialize()

    def _initialize(self) -> Redis:
        """ initialize class so red
        is connector for each db is only opened once
        """
        if self._socket:
            try:
                self.redis_db[self._db_key] = Redis(unix_socket_path=self._socket, db=self._db_key)
                self.redis_db[self._db_key].ping()
            except redis.exceptions.ConnectionError as e:
                self.redis_db[self._db_key] = Redis(host=self._host, port=self._port, db=self._db_key)
        else:
            self.redis_db[self._db_key] = Redis(host=self._host, port=self._port, db=self._db_key)
        return self.redis_db[self._db_key]

    def __call__(self, fn: Callable) -> Callable:
        """ create decorator for functions to use caching mechanism

        :param fn: function which returns value shall be cached or loaded from Cache
        :return: cache_func
        """

        @functools.wraps(fn)
        def cache_func(*args, **kwargs) -> Any:
            """ returns value from orig function either from cache or via orig function calls

            :param args: positional args from orig function
            :param kwargs: keyword args from orig function
            :return: value from cache or from fn call
            """

            if args:
                class_ = args[0] if inspect.isclass(args[0]) else args[0].__class__
                if hasattr(args[0], "logger"):
                    self.logger = args[0].logger
                else:
                    self.logger = self.get_logger()
            else:
                class_ = None
                self.logger = self.get_logger()

            # options how to build Cache-Path, difference between setter and getter,
            # so cached results can be reused for further refinement (e.g. autocomplete)
            # each class that uses a decorator can define it's own set and get function for the cache keys
            if class_ and hasattr(class_, "set_cache_key"):
                cache_getkey = class_.get_cache_key(*args, **kwargs, fn=fn)
                cache_setkey = class_.set_cache_key(*args, **kwargs, fn=fn)
            elif class_ and hasattr(class_, "get_cache_key"):
                cache_getkey = class_.get_cache_key(*args, **kwargs, fn=fn)
                cache_setkey = cache_getkey
            # otherwise a standard function to generate cache keys is used
            else:
                cache_getkey = self.get_cache_key(*args, **kwargs, fn=fn)
                cache_setkey = cache_getkey
                self.logger.debug("No Cache key function defined. Cache key " + cache_getkey + " built.")

            self.cache_getkey = cache_getkey
            self.cache_setkey = cache_setkey

            use_cache = self._cache_exists() and (self._cache or CacheLoader.not_connected())
            cache_date = self._get_cachetime()
            localtime = reference.LocalTimezone()

            # check if cache exist and file was modified after last caching
            data_updated = cache_date > self._last_change_date
            # cacheloader_start= if different cache_loader is used with shorter exp_time than originally set
            cacheloader_start = datetime.now(localtime) - timedelta(seconds=self._exp_time)
            cache_expired = cacheloader_start > cache_date

            # return cache and log reason for caching
            if (use_cache or data_updated) and not cache_expired \
                    and (self.decode_cache() or self.decode_cache() in self._store_empty):
                if data_updated:
                    cache_reason = "no changes"
                elif CacheLoader.not_connected():
                    cache_reason = "offline"
                elif self._cache:
                    cache_reason = "decorator setting"
                else:
                    cache_reason = "global setting"
                message_temp = "Cache used [{0}]: {1}, last changed: {2} > {3}"
                message = message_temp.format(cache_reason, self.cache_getkey, cache_date.isoformat(), self._last_change_date.isoformat())
                self.logger.info(message)
                if self.decode_cache():
                    try:
                        try:
                            return json.loads(self._get_cache().decode("utf-8"))
                        except (json.JSONDecodeError, UnicodeDecodeError):
                            unpickle = pickle.loads(self._get_cache())
                            return unpickle

                            # for backwards compatibility
                            # excluded, because get_keyvalue_cache (IIIF manifests does not use this)
                            try:
                                return json.loads(unpickle)
                            except Exception:
                                return unpickle
                    except Exception as e:
                        self.logger.critical("Cache Error: " + self.cache_getkey + " can not be opened: " + str(e))

            else:
                if CacheLoader.not_connected():
                    self.logger.info("No Cache, no connection: " + self.cache_getkey)
                # if no cache call function with arguments and set cache
                return_value = fn(*args, **kwargs)
                if return_value or return_value in self._store_empty:
                    self._set_cache(return_value)
                    self.logger.info("Cache-Entry created for " + self.cache_setkey)
                else:
                    self.logger.debug("No Cache created for " + self.cache_setkey + " because empty return value")
                return return_value

        return cache_func

    def decode_cache(self):
        return_value = None
        try:
            try:
                return_value = json.loads(self._get_cache().decode("utf-8"))
            except (json.JSONDecodeError, UnicodeDecodeError):
                return_value = pickle.loads(self._get_cache())

                # for backwards compatibility
                # excluded, because get_keyvalue_cache (IIIF manifests does not use this)
                try:
                    return_value= json.loads(return_value)
                except Exception:
                    pass
            return return_value
        except Exception as e:
            self.logger.critical("Cache Error: " + self.cache_getkey + " can not be opened: " + str(e))
            return return_value

    def get_cache_key(self, *args, fn: Callable ="", unique_id ="", **kwargs) -> str:
        """ build cache key from function arguments and function name
            shall be overwritten to return better cache-keys

        :param self: self can be used to access the object specific arguments from a function
        :param args: positional args from funct
        :param fn: function for which cache decorator shall be used
        :param kwargs: keyword args from funct
        :return: cache key built from object/functions arguments
        """
        if unique_id:
            cache_key = unique_id
        elif fn:
            fn = fn
            func_name = fn.__name__
            arg_names = inspect.getfullargspec(fn).args
            arg_dict = {}
            # get names for args using inspect function and store them in a dict
            for key, value in zip(arg_names, args):
                arg_dict[key] = value
            kwargs.update(arg_dict)
            try:
                del kwargs["self"]
            except KeyError:
                pass

            var_key = "_".join([key + "=" + str(CacheLoader.sort_json_dump(kwargs[key])) for key in sorted(kwargs.keys())])
            cache_key = func_name + "_" + var_key

        return cache_key

    @staticmethod
    def _write2disk(cache_key: str, value: str) -> None:
        """ store key - value pair as file on disk (key = dir and filename)

        :param cache_key: key / filepath + name where data shall be stored
        :param value: json serializable string to be stored
        """
        cache_file_path = os.path.join(CACHEPATH, cache_key)
        if not os.path.isdir(CACHEPATH):
            os.makedirs(CACHEPATH)
        if value:
            if not os.path.exists(os.path.dirname(cache_file_path)):
                os.makedirs(os.path.dirname(cache_file_path))
            with open(cache_file_path, "w") as f:
                f.write(value)

    def _loadfmdisk(self, key = None) -> str:
        """ load value from file from disk and set it within redis cache

        :return:
        """
        key = key or self.cache_setkey
        cache_file_path = os.path.join(CACHEPATH, key)
        with open(cache_file_path) as f:
            value = f.read()
            # todo: da data dump momentan nur als file rausgeschrieben wird
            # um redis cache nicht zu gross zu machen keine Verwendung
            # self._set_cache(value)
        return value

    def _cache_exists(self) -> bool:
        """check if cache for a key exists

        :return: True/False if cache for key exists
        """
        key = self.cache_getkey
        if self.r.exists(key):
            return True
        elif os.path.isfile(key) and os.stat(key).st_size != 0:
            return True
        else:
            return False

    def _get_cache(self) -> str:
        """ get value for key from cache or disk

        :return: string that can be used for json.loads()
        """
        key = self.cache_getkey
        if self.r.exists(key):
            if self._exp_time:
                self.r.expire(key, self._exp_time)
            return self.r.get(key)
        elif os.path.isfile(key) and os.stat(key).st_size != 0:
            return self._loadfmdisk()
        else:
            self.logger.warning("Cache could not be loaded: " + key)
            return None

    def _get_cachetime(self) -> datetime:
        """ get cachetime for a specific key either from redis or from unix mtime,
            if no cache return date in the future

        :return: datetime object
        """
        key = self.cache_getkey
        time_key = "cachetime_" + key
        localtime = reference.LocalTimezone()

        if self.r.exists(time_key):
            time_value = self.r.get(time_key).decode("utf-8")
            return self.laststrdate2date(time_value)
        elif os.path.isfile(key) and os.stat(key).st_size != 0:
            return datetime.fromtimestamp(os.path.getmtime(key), localtime)
        else:
            return self.laststrdate2date("1900-01-01 01:00:00.0Z")

    def read_redis_channels(self) -> None:
        """ not used, to trigger actions when redis events occure"""
        self.r.config_set("notify-keyspace-events", "KEA")
        pubsub = self.r.pubsub()
        pubsub.psubscribe("__keyevent@*__:set")

    def _set_cache(self, value: Any) -> None:
        """ set redis cache for any json serializable value (optional with expiration time),
            store also datetime in redis when object was stored
            if data_dump (big dataset e.g. from ES write it also to disk so it will not be removed by Lru Cache)

        :param value: datatype to be transformed to json
        """
        key = self.cache_setkey
        now = datetime.utcnow().isoformat()
        time_key = "cachetime_" + key

        # check if already pickle serialized value
        try:
            json.loads(value)
            value = pickle.dumps(value)
        except (pickle.UnpicklingError, TypeError, json.JSONDecodeError):
            try:
                value = json.dumps(value)
            except TypeError:
                value = pickle.dumps(value)

        if CACHE_DB_DICT.get(self._db_key, "") == "data_dump":
            self._write2disk(key, value)
        else:
            if self._exp_time:
                self.r.setex(name=key, value=value, time=self._exp_time)
                self.r.setex(name=time_key, value=now, time=self._exp_time)
            else:
                self.r.set(key, value)
                self.r.set(time_key, now)

    def get_keyvalue_cache(self, key: Any) -> str:
        """ get value for key from cache or disk

        :return: string that can be used for json.loads()
        """
        if self.r.exists(key):
            if self._exp_time:
                self.r.expire(key, self._exp_time)
            try:
                try:
                    json.loads(self.r.get(key).decode("utf-8"))
                    return self.r.get(key).decode("utf-8")
                except (json.JSONDecodeError, UnicodeDecodeError):
                    unpickle = pickle.loads(self.r.get(key))
                    # for backwards compatibility
                    # excluded, because get_keyvalue_cache (IIIF manifests does not use this)
                    try:
                        return json.loads(unpickle)
                    except Exception:
                        return unpickle
            except Exception as e:
                self.logger.critical("Cache Error: " + self.cache_getkey + " can not be opened: " + str(e))
                return ""
        elif os.path.isfile(key) and os.stat(key).st_size != 0:
            return self._loadfmdisk(key)
        else:
            try:
                self.logger.warning("Cache could not be loaded: " + key)
            except AttributeError:
                self.logger = self.get_logger()
                self.logger.warning("Cache could not be loaded: " + key)
            return ""

    def set_keyvalue_cache(self, value: Any, key: Any) -> None:
        """ set redis cache for any json serializable value (optional with expiration time),
            store also datetime in redis when object was stored
            if data_dump (big dataset e.g. from ES write it also to disk so it will not be removed by Lru Cache)

        :param value: datatype to be transformed to json
        """
        now = datetime.utcnow().isoformat()
        time_key = "cachetime_" + key

        # check if already pickle serialized value
        try:
            json.loads(value)
            value = pickle.dumps(value)
        except (pickle.UnpicklingError, TypeError, json.JSONDecodeError):
            try:
                value = json.dumps(value)
            except TypeError:
                value = pickle.dumps(value)

        if CACHE_DB_DICT.get(self._db_key, "") == "data_dump":
            self._write2disk(key, value)
        else:
            if self._exp_time:
                self.r.setex(name=key, value=value, time=self._exp_time)
                self.r.setex(name=time_key, value=now, time=self._exp_time)
            else:
                self.r.set(key, value)
                self.r.set(time_key, now)

    @staticmethod
    def laststrdate2date(date_str: str) -> datetime:
        """ transform str to datetime object with correct timezone

        :param date_str: datetime str in isoformat
        :return: datetime object with correct timezone
        """
        # 2018-09-19T10:07:55.192605 to 2018-09-19 09:58:00.361283 (date.now.isoformat)
        date_str = date_str.replace("T", " ")
        date_str = date_str.split(".")[0]
        date = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
        date = date.replace(tzinfo=timezone.utc).astimezone(tz=None)
        return date

    @classmethod
    def sort_json_dump(cls, value: Any) -> str:
        """ order nested dictionaries

        :param value: data structure to be sorted
        :return: string serialized json
        """

        if isinstance(value, list):
            a_dump = []
            for a in value:
                # to exclude None or "" Values
                if a == 0 or a:
                    a_copy = cls.sort_json_dump(a)
                    a_dump.append(a_copy)
            a_dump.sort()
        elif isinstance(value, dict):
            a_dump = {}
            for key, value in sorted(value.items()):
                if cls.is_simple_field(value):
                    a_dump[key] = str(value)
                else:
                    a_dump[key] = cls.sort_json_dump(value)
        else:
            if value != 0 and not value:
                # to exclude None values
                value = ""
            if not isinstance(value, str):
                value = str(value)
            return value
        # otherwise empty a_dumps end up as [], {}-String
        if a_dump:
            return json.dumps(a_dump, sort_keys=True, ensure_ascii=False)

    @staticmethod
    def is_simple_field(value):
        """

        :param value:
        :return:
        """
        return isinstance(value, int) or isinstance(value, float) or isinstance(value, str) \
               or isinstance(value, type(None))


    @staticmethod
    def get_logger():

        config = yaml.load(open(resource_filename(Requirement.parse("cache_decorator_redis_ubit"), "cache_decorator_redis_ubit/cfg/logger_cache.yaml")), Loader=yaml.Loader)
        logging.config.dictConfig(config)
        if MODE in ["prod", "test"]:
            return logging.getLogger('cache_prod')
        else:
            return logging.getLogger('cache_debug')

    @classmethod
    def not_connected(cls) -> bool:
        """ check if an internet connection is available

        :return: True if connection is available and no further connection tests (cls.connection_tested -> True),
            otherwise set use_cache to True
        """
        if cls.use_cache:
            return True
        elif cls.connection_tested:
            return False
        else:
            try:
                # see if we can resolve the host name -- tells us if there is
                # a DNS listening
                host = socket.gethostbyname(REMOTE_SERVER)
                # connect to the host -- tells us if the host is actually
                # reachable
                socket.create_connection((host, 80), 1)
                cls.connection_tested = True
                return False
            except (socket.gaierror, socket.timeout, socket.herror) as e:
                logger = cls.get_logger()
                logger.warning("No connection. Using cache: " + str(e))
                cls.use_cache = True
                return True
            except:
                import traceback
                traceback.print_exc()
